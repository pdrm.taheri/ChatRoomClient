//
// Created by pedram on 3/15/17.
//

#ifndef CHATROOMCLIENT_DATASTORE_H
#define CHATROOMCLIENT_DATASTORE_H


#include <QtCore/QByteArray>
#include <string>
#include "Protobuf/user.pb.h"

class DataStore
{
public:
    static DataStore *getInstance();

    std::vector<Message *> &getMessages();
    void addMessage(Message *msg);

    std::string getMyName();
    void setMyName(std::string myName);

private:
    DataStore();
    ~DataStore();

    std::string myName;

    std::vector<Message *> messages;
};

#endif //CHATROOMCLIENT_DATASTORE_H
