//
// Created by pedram on 3/15/17.
//

#include "DataStore.h"

// Definition of static variables
static DataStore *instance;

DataStore *DataStore::getInstance()
{
    if (instance == nullptr)
    {
        instance = new DataStore();
    }

    return instance;
}

DataStore::DataStore()
{
}

DataStore::~DataStore()
{
    for (auto message : messages)
    {
        delete message;
    }

    delete instance;
}

std::string DataStore::getMyName()
{
    return myName;
}

void DataStore::setMyName(std::string myName)
{
    this->myName = myName;
}

std::vector<Message *> &DataStore::getMessages()
{
    return messages;
}

void DataStore::addMessage(Message *msg)
{
    messages.push_back(msg);
}
