//
// Created by pedram on 4/18/17.
//

#ifndef CHATROOMCLIENT_PARSER_H
#define CHATROOMCLIENT_PARSER_H


#include <QtCore/QByteArray>
#include "../Model/Protobuf/user.pb.h"

class Parser
{
public:
    static Message *parseFromBytes(const QByteArray bytes);
    static Message *parseFromFields(const std::string &name, const char *content);
    static QByteArray serializeFromMessage(const Message *message);
};


#endif //CHATROOMCLIENT_PARSER_H
