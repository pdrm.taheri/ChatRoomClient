//
// Created by pedram on 4/18/17.
//

#include "Parser.h"


///////////////////////// Parse Methods /////////////////////////

Message *Parser::parseFromBytes(const QByteArray bytes)
{
    Message *message = new Message();
    message->ParseFromArray(bytes.data(), bytes.size());

    return message;
}

Message *Parser::parseFromFields(const std::string &name, const char *content)
{
    Message *message = new Message();
    message->set_content(content);
    message->set_name(name);

    return message;
}


///////////////////////// Serialize Methods /////////////////////////

QByteArray Parser::serializeFromMessage(const Message *message)
{
    return QByteArray(message->SerializeAsString().c_str(), message->ByteSize());
}