//
// Created by pedram on 3/15/17.
//

#ifndef CHATROOMCLIENT_CONNECTIONHANDLER_H
#define CHATROOMCLIENT_CONNECTIONHANDLER_H


#include <QtNetwork/QTcpSocket>

class ConnectionHandler : public QObject
{
public:
    static ConnectionHandler *getInstance();

    // Events and handlers
    void onNewWriteOnSocket();
    void onSocketClosed();
    void onSocketOpened();

    void setOnNewMessageHandler(void (*handler)(QByteArray message));
    void setOnConnectionClosedHandler(void (*handler)());
    void setOnConnectionEstablishedHandler(void (*handler)());

    void connectToHost(QHostAddress &hostAddress, unsigned short port);

    void writeOnSocket(QByteArray message);

private:
    ConnectionHandler();
    ~ConnectionHandler();

    void (*onNewMessageHandler)(QByteArray message);
    void (*onConnectionClosedHandler)();
    void (*onConnectionEstablishedHandler)();

    QTcpSocket *connection;
};


#endif //CHATROOMCLIENT_CONNECTIONHANDLER_H
