//
// Created by pedram on 3/15/17.
//

#include "Client.h"


// Definition of static variables
static Client *instance;

Client::Client()
{
    ConnectionHandler::getInstance()->setOnConnectionClosedHandler(Client::onDisconnect);
    ConnectionHandler::getInstance()->setOnConnectionEstablishedHandler(Client::onConnect);
    ConnectionHandler::getInstance()->setOnNewMessageHandler(Client::onNewMessage);
}

Client::~Client()
{
    delete instance;
}

void Client::onNewMessage(QByteArray message)
{
    qDebug() << "New message received " << message;
    Message *newMessage = Parser::parseFromBytes(message);

    qDebug() << "Sender: " << newMessage->name().c_str();
    qDebug() << "Content: " << newMessage->content().c_str();

    DataStore::getInstance()->addMessage(newMessage);
    emit getInstance()->newMessageArrive();
}

void Client::onDisconnect()
{

}

void Client::onConnect()
{

}

Client *Client::getInstance()
{
    if (instance == nullptr)
    {
        instance = new Client();
    }

    return instance;
}

void Client::run()
{
    getInstance(); // To initialize handlers

    QHostAddress address(HOST_ADDRESS);
    ConnectionHandler::getInstance()->connectToHost(address, HOST_PORT);
}

void Client::sendMessage(const QByteArray message)
{
    ConnectionHandler::getInstance()->writeOnSocket(message);
}

void Client::sendMessage(const Message *message)
{
    getInstance()->sendMessage(Parser::serializeFromMessage(message));
}

bool Client::sendMessage(const std::string &text)
{
    qDebug() << "Sending message";
    qDebug() << "Start of serialization";

    if (!Utility::isValidToSend(text.c_str()))
    {
        qDebug() << "Invalid text, aborting send operation";
        return false;
    }

    // Build a message object
    Message *message = Parser::parseFromFields(DataStore::getInstance()->getMyName(),
                                               Utility::getTrimmedString(text).toStdString().c_str());

    qDebug() << "End of serialization, results:" << "Content:" << message->content().c_str() << "Sender:"
             << message->name().c_str();

    sendMessage(message);

    return true;
}

