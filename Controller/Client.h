//
// Created by pedram on 3/15/17.
//

#ifndef CHATROOMCLIENT_CLIENT_H
#define CHATROOMCLIENT_CLIENT_H


#include <QtNetwork/QHostAddress>
#include <QtCore/QByteArray>
#include "../Model/DataStore.h"
#include "ConnectionHandler.h"
#include "../Constants.h"
#include "../Model/Protobuf/user.pb.h"
#include "Parser.h"
#include "../Utility.h"


class Client : public QObject
{
Q_OBJECT

public:
    static void onDisconnect();
    static void onConnect();

    static Client *getInstance();
    static void run();

    static void sendMessage(const QByteArray message);
    static void sendMessage(const Message *message);
    static bool sendMessage(const std::string &message);
    static void onNewMessage(QByteArray message);

signals:
    void newMessageArrive();

private:
    Client();
    ~Client();
};


#endif //CHATROOMCLIENT_CLIENT_H
