//
// Created by pedram on 4/18/17.
//

#ifndef CHATROOMCLIENT_UTILITY_H
#define CHATROOMCLIENT_UTILITY_H


#include <QtCore/QString>

class Utility
{
public:
    static bool isValidToSend(const QString &message);
    static QString getTrimmedString(const QString &message);
    static QString getTrimmedString(const std::string &message);

    static bool isValidName(const QString &name);
};


#endif //CHATROOMCLIENT_UTILITY_H
