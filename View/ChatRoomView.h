//
// Created by pedram on 3/15/17.
//

#ifndef CHATROOMCLIENT_CHATROOMVIEW_H
#define CHATROOMCLIENT_CHATROOMVIEW_H


#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>
#include <QtGui>
#include "../Utility.h"
#include "../Controller/Client.h"
#include "../Model/DataStore.h"

class ChatRoomView : public QObject
{
public:
    ChatRoomView();
    ~ChatRoomView();

    void show();

public slots:
    void onNewMessageHandler();

public slots:
    void start();

private:
    QTextEdit *messageTextEdit;
    QPushButton *sendButton;
    QHBoxLayout *inputAccessoryViewsLayout;

    QListWidget *messagesView;

    QVBoxLayout *mainLayout;

    QWidget *widget;

    void sendButtonClicked();

};


#endif //CHATROOMCLIENT_CHATROOMVIEW_H
