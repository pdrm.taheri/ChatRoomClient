//
// Created by pedram on 3/15/17.
//

#include "LoginView.h"

LoginView::LoginView()
{
    layout = new QVBoxLayout();
    submit = new QPushButton("اتصال");
    nameLineEdit = new QLineEdit();
    nameLineEdit->setPlaceholderText("نام خود را وارد کنید");

    layout->addWidget(nameLineEdit);
    layout->addWidget(submit);

    mainLayout = new QVBoxLayout();
    mainLayout->addLayout(layout);

    widget = new QWidget();

    widget->setLayout(mainLayout);

    widget->setWindowTitle("ورود");

    connect(submit, &QPushButton::clicked, this, &LoginView::submitButtonClicked);
}

LoginView::~LoginView()
{
    delete nameLineEdit;
    delete submit;
    delete layout;
    delete mainLayout;
    delete widget;
}

void LoginView::show()
{
    widget->show();
}

void LoginView::submitButtonClicked()
{
    if (Utility::isValidName(nameLineEdit->text()))
    {
        DataStore::getInstance()->setMyName(Utility::getTrimmedString(nameLineEdit->text()).toStdString());
        Client::getInstance()->run();

        emit loginSuccess();
        widget->close();
    }
    else
    {
        nameLineEdit->setText("");
    }
}
