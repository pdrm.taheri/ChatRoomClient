//
// Created by pedram on 3/15/17.
//

#ifndef CHATROOMCLIENT_LOGINVIEW_H
#define CHATROOMCLIENT_LOGINVIEW_H


#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtCore/QEventLoop>
#include "../Controller/ConnectionHandler.h"
#include "../Controller/Client.h"
#include "../Model/DataStore.h"

class LoginView : public QObject
{
Q_OBJECT

public:
    LoginView();
    ~LoginView();

    void show();

signals:
    void loginSuccess();

private:
    QLineEdit *nameLineEdit;
    QPushButton *submit;

    QVBoxLayout *layout;

    QVBoxLayout *mainLayout;

    QWidget *widget;

    void submitButtonClicked();
};


#endif //CHATROOMCLIENT_LOGINVIEW_H
