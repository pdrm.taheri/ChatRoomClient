//
// Created by pedram on 3/15/17.
//

#include "ChatRoomView.h"


ChatRoomView::ChatRoomView()
{
    messageTextEdit = new QTextEdit();
    messageTextEdit->setPlaceholderText("پیام خود را برای ارسال وارد کنید");

    sendButton = new QPushButton("ارسال");

    inputAccessoryViewsLayout = new QHBoxLayout();
    inputAccessoryViewsLayout->addWidget(sendButton);
    inputAccessoryViewsLayout->addWidget(messageTextEdit);

    messagesView = new QListWidget();

    mainLayout = new QVBoxLayout();
    mainLayout->addWidget(messagesView);
    mainLayout->addLayout(inputAccessoryViewsLayout);

    widget = new QWidget();
    widget->setLayout(mainLayout);

    connect(sendButton, &QPushButton::clicked, this, &ChatRoomView::sendButtonClicked);
    connect(Client::getInstance(), &Client::newMessageArrive, this, &ChatRoomView::onNewMessageHandler);
}

ChatRoomView::~ChatRoomView()
{
    delete messageTextEdit;
    delete sendButton;
    delete messagesView;
    delete inputAccessoryViewsLayout;
    delete mainLayout;
    delete widget;
}

void ChatRoomView::show()
{
    widget->show();
}

void ChatRoomView::sendButtonClicked()
{
    qDebug() << "Send button clicked, sending...";

    QString toSend = Utility::getTrimmedString(messageTextEdit->toPlainText());
    messageTextEdit->setText("");

    if (!Utility::isValidToSend(toSend))
    {
        qDebug() << "Invalid text, aborting send";
        return;
    }

    Client::getInstance()->sendMessage(toSend.toStdString());
    QListWidgetItem *item = new QListWidgetItem(("Me : " + toSend.toStdString()).c_str());
    item->setTextAlignment(Qt::AlignmentFlag::AlignRight);
    item->setBackgroundColor(QColor::fromRgb(18, 200, 3));

    messagesView->addItem(item);
    messagesView->scrollToBottom();
}

void ChatRoomView::onNewMessageHandler()
{
    Message *message = DataStore::getInstance()->getMessages().back();

    system(("notify-send \"" + message->name() + "\" \"" + message->content() + "\"").c_str());
    messagesView->addItem(new QListWidgetItem((message->name() + ": " + message->content()).c_str()));
}

void ChatRoomView::start()
{
    widget->setWindowTitle(DataStore::getInstance()->getMyName().c_str());

    show();
}
