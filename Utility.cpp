//
// Created by pedram on 4/18/17.
//

#include "Utility.h"

bool Utility::isValidToSend(const QString &message)
{
    return !message.trimmed().isEmpty();
}

QString Utility::getTrimmedString(const std::string &message)
{
    return QString(message.c_str()).trimmed();
}

QString Utility::getTrimmedString(const QString &message)
{
    return message.trimmed();
}

bool Utility::isValidName(const QString &name)
{
    return !name.trimmed().isEmpty();
}
