#include <QtWidgets/QApplication>
#include "View/LoginView.h"
#include "View/ChatRoomView.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    LoginView *loginView = new LoginView();

    ChatRoomView *chatRoomView = new ChatRoomView();

    QObject::connect(loginView, &LoginView::loginSuccess, chatRoomView, &ChatRoomView::start);

    loginView->show();

    return app.exec();
}